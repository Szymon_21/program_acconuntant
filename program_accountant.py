print(30*'#')
print('PROGRAM ACCOUNTANT EXTENDED')
print(30*'#')
print('\n')

acount = {}
money = 0

history_of_buy = {}
history_of_sell = {}

depot = {}
depot_moment = []
depot_product = []

class Balance:
    def __init__(self, value, coment):
        self.value = value
        self.coment = coment

    def save_to_dict(self):
        self.value = int(self.value) * 100
        money = int(money)
        money_action = money + self.value
        acount[self.coment] = self.value
        print(f'na koncie mamy {money_action} groszy')
        
class Sell:
    def __init__(self, product_id, price, number):
        self.product_id = product_id
        self.price = price
        self.number = number
    
    def save_to_dict(self):    
        self.price = int(self.price)
        self.number = int(self.number)
            
        price_of_all = self.price * self.number
        
        action1 = money + price_of_all
            
        if depot(self.product_id) < self.number:
            print('Nie ma tyle towaru na magazynie!')
            print('Spróbuj inną kwotę.')
                
        else:
            numer_of_product_in_depot = depot(self.product_id)
                
            action_sell = numer_of_product_in_depot - self.number
                
            depot[self.product_id] = action_sell
                
            depot_product.remove(self.product_id)
                
            money = action1
         
class Buy:
    def __init__(self, product_id, price, number):
        self.product_id = product_id
        self.price = int(price)
        self.number = int(number)

    def save_to_dict(self): 
        action2 = money - (self.price * self.number)
        money = action2
        
        depot_moment.append(self.price, self.number)
        depot[self.product_id] = depot_moment
        
        total_money_for_products = self.price * self.number
        history_of_buy[self.product_id] = total_money_for_products
          
while True:
    
    ACCESIBLE_COMAND = ("saldo", "zakup", "sprzedaż",
                        "konto", "magazyn", "przegląd",
                        "koniec")
    
    print('\n')
    print(11 * '#')
    print('MENU GŁÓWNE')
    print(11 * '#')
    print('\n')

    print(f'Dostepne komendy: {ACCESIBLE_COMAND}')
    task = input('Podaj komendę: ')

    if task == '':
        print('Brak komendy.')
        print('Komendy które można używać:')
        print('\n'.join(ACCESIBLE_COMAND))
        input('wciśnij <ENTER> aby spróbować ponownie. \n')
            
    elif task not in ACCESIBLE_COMAND :
        print('Komenda nie pasuje do żadnej z zapisanych.')
        print('Komendy które można używać:')
        print('\n'.join(ACCESIBLE_COMAND))
        input('wciśnij <ENTER> aby spróbować ponownie. \n')

    if task == "koniec":
        print('Brak komendy.')
        print('Komendy które można używać:')
        print('\n'.join(ACCESIBLE_COMAND))

    elif task == "saldo" :
        print(15 * '-')
        print('SALDO')
        print(15 * '-')

        while True:
            value = input('Podaj Kwotę albo nacisnij enter aby wyjść: ')
            value = int(value)
            
            if value == '':
                break
            
            else:
                try:
                    coment = input('Podaj komentarz do kwoty: ')
                    balance_input = Balance(value, coment)
                    balance_input.save_to_dict()
                    continue
                
                except ValueError:
                    print('Podana wartość nie jest liczbą.')
                    break
            
    elif task == "sprzedaż":
        print(15 * '-')
        print('SPRZEDAŻ')
        print(15 * '-')

        while True:
            product_id = input('Podaj identyfikator produktu albo nacisnij enter aby wyjść: ')
            if product_id == '':
                break
            
            else:
                if product_id not in depot:
                    print('\n')
                    print('Nie można sprzedać takiego produktu, ponieważ nie ma go w magazynie.')
                    print('Powrót do głównego menu.')
                    print('\n')
                    break
                
                else:
                    price = input('Podaj kwotę sprzedanych produktów: ')
                    number = input('podaj ilość sprzedanych produktów: ')
                    sell_input = Sell(product_id, price, number)
                    sell_input.save_to_dict()
                    continue
            
    elif task == "zakup":
        print(15 * '-')
        print('ZAKUP')
        print(15 * '-')

        while True:
            product_id = input('Podaj identyfikator produktu albo nacisnij enter aby wyjść: ')
            if product_id == '':
                break
            
            else:
                price = input('Podaj kwotę kupionych produktów: ')
                number = input('podaj ilość kupionych produktów: ')
                buy_input = Buy(product_id, price, number)
                buy_input.save_to_dict()
                continue

    elif task == "konto":
        print(15 * '-')
        print('KONTO')
        print(15 * '-')
        
        print(acount)
        print(money)

    elif task == "magazyn":
        print(15 * '-')
        print('MAGAZYN')
        print(15 * '-')

        while True:
            product_id = input('Podaj nazwę produktu: ')
            #product_value = input('Podaj ilość: ')
            if product_id == '':
                break
            
            else:
                depot_product.append(product_id)
                print(f'dodaliśmy do magazynu {product_id}')
                
    elif task == "przegląd":
        print(15 * '-')
        print('PRZEGLĄD')
        print(15 * '-')
        
        print(history_of_buy)
        print(history_of_sell)
        print(depot)